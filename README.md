# zabbix

## Make 2 scripts:
- for discovery all metrics
- for every metric value

## Connect scripts via UserParameter to Zabbix agent
- zabbix-agentd conf

## Set LLD & triggers & actions (Telegram)
- create item/trigger/graph prototypes (see screenshots of graphs and telegram's messages)